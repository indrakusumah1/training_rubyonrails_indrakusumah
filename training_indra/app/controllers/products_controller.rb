class ProductsController < ApplicationController
	
  def index    
    @product = Product.all  
    @lastproduct = Product.maximum(:id)
    @lastproduct = Product.find(@lastproduct)
  end

  def destroy
    if @product = Product.find(params[:id])
    @product.destroy
    redirect_to :action => :index
    end

  end

  def edit
    @product = Product.find(params[:id])
  end

  def update
    
      @product = Product.find(params[:id]) 
      if @product.update_attributes(params[:product])
      	if @product.save
         flash[:update] = "You was update 1 product"
         redirect_to :action => :index
        end
      else
         render :action => 'edit'
      end

    end

  def show
    @product = Product.find(params[:id])
  end

  def new
    @product = Product.new
  end

  def create
    @product = Product.new(params[:Product])
    if @product.save
      redirect_to :action => :index
    else
      render :action => 'new'
    end

  end
end
