class UsersController < ApplicationController

def index    
    @user = User.all
    @lastuser = User.maximum(:id)
    @lastuser = User.find(@lastuser)
  end

  def destroy
    @user = User.find(params[:id])
    if @user.destroy
     flash[:delete] = "You was delete 1 user"
     redirect_to :action => :index
    end

  end

  def edit
    @user = User.find(params[:id])
  end

  def update
      @user = User.find(params[:id]) 
      if @user.update_attributes(params[:user])
         flash[:update] = "You was update 1 user"
         redirect_to :action => :index
      else
       render :action => 'edit'
      end

    end

  def show
    @user = User.find(params[:id])
  end

  def new
    @user = User.new
  end
  

  def create
    @user = User.new(params[:user])
    if verify_recaptcha
     if @user.save
      UserMailer.registration_confirmation(@user).deliver
      flash[:notice] = "You was add 1 user"
      redirect_to root_url, :notice => "Signed up!"
      return
     end
    else
      render :action=>'new'
    end
  end
  
end
