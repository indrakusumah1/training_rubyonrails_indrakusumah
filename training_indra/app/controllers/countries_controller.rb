class CountriesController < ApplicationController

  def index    
    @country = Country.all  
    @lastcountry = Country.maximum(:id)
    @lastcountry = Country.find(@lastcountry)
  end

  def destroy
    @country = Country.find(params[:id])
    if @country.destroy
     flash[:delete] = "You was delete 1 country"
     redirect_to :action => :index
    end
  end

  def edit
    @country = Country.find(params[:id])
  end

  def update
    
    @country = Country.find(params[:id]) 
    if @country.update_attributes(params[:Country])
      if @product.save
       flash[:notice] = "You was update 1 country"
       redirect_to :action => :index
      end
      else
        render :action => 'edit'
      end

    end

  def show
    if @country = Country.find(params[:id])
      render :action=>'show'
    else
      redirect_to country_path
    end
  end

  def new
    @country = Country.new
  end

  def create
    @country = Country.new(params[:Country])
    if @country.save
      redirect_to :action => :index
      flash[:notice] = "You was succesfully create 1 country"
    else
      render :action => 'new'
    end

  end
end
