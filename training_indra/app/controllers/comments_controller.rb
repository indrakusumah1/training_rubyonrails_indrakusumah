class CommentsController < ApplicationController
  def index    
    @comment = Comment.all
    @lastcomment = Comment.maximum(:id)
    @lastcomment = Comment.find(@lastcomment)
  end

  def destroy
    @comment = Comment.find(params[:id])
    if @comment.destroy
     flash[:delete] = "You was delete 1 comment"
     redirect_to :action => :index
    end

  end

  def edit
    @comment = Comment.find(params[:id])
  end

  def update
      @comment = Comment.find(params[:id]) 
      if @comment.update_attributes(params[:comment])
         flash[:update] = "You was update 1 comment"
         redirect_to :action => :index
      else
       render :action => 'edit'
      end

    end

  def show
    @comment = Comment.find(params[:id])
  end

  def new
    @comment = Comment.new
  end

  def create
    @comment = Comment.new(params[:comment])
    if @comment.save
      flash[:notice] = "You was add 1 comment with id :"
      redirect_to :action=>'index'
    else
      render :action=>'new'
    end
  end
end
