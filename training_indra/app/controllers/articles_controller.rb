class ArticlesController < ApplicationController

  def index    
    @article = Article.all
    @lastarticle = Article.maximum(:id)
    @lastarticle = Article.find(@lastarticle)
  end

  def destroy
    @article = Article.find(params[:id])
    if @article.destroy
     flash[:delete] = "You was delete 1 article"
     redirect_to :action => :index
    end

  end

  def edit
    @article = Article.find(params[:id])
  end

  def update
      @article = Article.find(params[:id]) 
      if @article.update_attributes(params[:article])
         flash[:update] = "You was update 1 article"
         redirect_to :action => :index
      else
       render :action => 'edit'
      end

    end

  def show
    @article = Article.find(params[:id])
  end

  def new
    @article = Article.new
  end

  def create
    @article = Article.new(params[:article])
    if @article.save
      flash[:notice] = "You was add 1 article"
      redirect_to :action=>'index'
    else
      render :action=>'new'
    end

  end
 
  before_filter :require_login, :only => [:new, :create, :edit, :update, :delete]
  
end
