require 'bcrypt'

class User < ActiveRecord::Base
  attr_accessible :first_name, :last_name , :email, :date_of_birth, :user_name, :password, :age, :address, :countries_id , :password_hash, :password_salt, :password_confirmation
  has_many :articles
  belongs_to :countries
  has_many :comments
  scope :indonesian, where("countries_id = '1'")
  #Override relation
  has_many :artikel,
           :class_name => "Article" ,
           :foreign_key => "user_id" ,
           :conditions => "user_id = '1'"
  #Validation
  validates :first_name,  :length => {:minimum => 1, :maximum => 25},
                          :uniqueness => true,
                          :format => {:with => /[a-zA-Z\s]+$/},
 			                    :presence => {:message=>"you must be filled username"}
                          

  validates :last_name,   :length => {:minimum => 1, :maximum => 25},
                          :uniqueness => true,
                          :format => {:with => /[a-zA-Z\s]+$/},
 			                    :presence => {:message=>"you must be filled username"}
                          

  validates :email,  :length => {:minimum => 1, :maximum => 25},
                     :uniqueness => true,
		                 :presence => {:message=>"you must be filled username"}

  #attr_accessor :password
  #validates :password, :presence => {:on => :create},
   #                  :confirmation => true
  validates :email, :presence => true, :uniqueness => true



  validates :date_of_birth,  :presence => {:message=>"you must be filled username"}
  validates :user_name,  :presence => {:message=>"you must be filled username"}
  validates :age,  :presence => {:message=>"you must be filled username"}
  validates :address,  :presence => {:message=>"you must be filled username"}

  
           
               
  before_save :encrypt_password

  validates :password, :presence => {:on => :create},
                       :confirmation => true
  validates :email, :presence => true, :uniqueness => true

  def encrypt_password
     if password.present?
          self.password_salt = BCrypt::Engine.generate_salt
          self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
     end
  end




  def get_full_address
     negara=Country.find(self.countries_id)
    "#{self.address} #{negara.name}"
  end
  def show_full_address
    "#{self.first_name} #{self.last_name}"
  end
  def self.show_all_user
    age=User.where("age <= 18")
  end
  def self.authenticate(email, password)
  user = find_by_email(email)
  if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
    user
  else
    nil
  end
end

   
end
