class Article < ActiveRecord::Base
   attr_accessible :title, :text
   has_many :comments, :dependent=> :destroy 
   belongs_to :users
   scope :artikel, lambda {|articles| where("rating = ?", artikel) }
   #Validation
   validates :title,  :presence => {:message=>"you must be filled username"},
                      :uniqueness => true

   #validate :validtitle
   validates :title,  :presence => {:message=>"you must be filled username"}

   def validtitle
    self.errors[:title] << "can't filled by 'blank'/'empty'/'null'" if title == 'blank' or 'empty' or 'null'
   end  
                      
   
   def self.show_all_article
    where("length(text) <= 100")
   end

end
