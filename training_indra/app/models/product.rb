class Product < ActiveRecord::Base
  attr_accessible :name, :price, :stock, :description
  belongs_to :users
  scope :stok, lambda {|stoks| where("stock = ?", stoks) }
  scope :harga, where("price <= 1000")
  #Validation
  validates :price,      :numericality => true
  validates :name,  :presence => {:message=>"you must be filled username"}
  validates :price,  :presence => {:message=>"you must be filled username"}
  validates :stock,  :presence => {:message=>"you must be filled username"}
  validates :description,  :presence => {:message=>"you must be filled username"}

end
