class CountryId < ActiveRecord::Migration
  def up
  add_column :users, :countries_id, :string
  end

  def down
   remove_column :users, :countries_id, :string
  end
end
