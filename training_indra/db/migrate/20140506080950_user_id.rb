class UserId < ActiveRecord::Migration
  def up
  add_column :articles, :user_id, :string
  end

  def down
  remove_column :articles, :user_id, :string
  end
end
