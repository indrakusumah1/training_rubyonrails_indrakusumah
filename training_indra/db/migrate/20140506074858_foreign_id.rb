class ForeignId < ActiveRecord::Migration
  def up
   add_column :users, :category_id, :string
   add_column :users, :article_id, :string
  end

  def down
  remove_column :users, :category_id, :string
  remove_column :users, :article_id, :string
  end
end
