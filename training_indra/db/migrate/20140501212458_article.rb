class Article < ActiveRecord::Migration
  def change
  create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :user_name
      t.string :password
      t.string :bio_profile
  
  end

  
  create_table :countries do |t|
      t.integer :code
      t.string :name
  end
  

  
  create_table :articles do |t|
      t.string :title
      t.string :body
  end
  

  
  create_table :comments do |t|
      t.string :body
  end
  end
end
