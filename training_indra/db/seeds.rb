# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ :name => 'Chicago' }, { :name => 'Copenhagen' }])
#   Mayor.create(:name => 'Emanuel', :city => cities.first)

def add_user(n1,n2,emel,username,pass,bio)
  users = User.create([{ :first_name=>n1, :last_name=>n2, :email=>emel, :user_name=>username, :password=>pass, :bio_profile=>bio}])
end

  add_user("Indra","Kusumah","123@gmail.com","indra","qwe","free")
  add_user("Andra","Kusumah","234@gmail.com","andra","qwe","free")
  add_user("Indri","Kusumah","345@gmail.com","indri","qwe","free")
  add_user("Indro","Kusumah","678@gmail.com","indro","qwe","free")
  add_user("Andri","Kusumah","890@gmail.com","andri","qwe","free")

def add_country(kode,nama)
  country = Countrie.create([{ :code=>kode, :name=>nama}])
end

  add_country("+62","Indonesia")
  add_country("+52","Malay")
  add_country("+1","Amerika")
  add_country("+2","Israel")
  add_country("+32","Estonia")

def add_artikel(judul,isi)
  artikel = Article.create([{ :title=>judul, :text=>isi}])
end

  add_artikel("aa","Indonesiamnkladmk")
  add_artikel("bb","Malaydwadwadwa")
  add_artikel("cc","Amerdawdawdika")
  add_artikel("dd","Israwdawdwadael")
  add_artikel("ee","Estawdwadwaonia")

def add_komen(komens)
  komen = Comment.create([{ :body=>komens}])
end

  add_komen("aaIndonesiamnkladmk")
  add_komen("bbMalaydwadwadwa")
  add_komen("ccAmerdawdawdika")
  add_komen("ddIsrawdawdwadael")
  add_komen("eeEstawdwadwaonia")

def add_produk(n1,n2,n3,n4)
  produks = Product.create([{ :name=>n1, :price=>n2, :stock=>n3, :description=>n4}])
end

  add_produk("Aibon","1000","30","lem")
  add_produk("Sabun","1000","30","lem")
  add_produk("Shampo","1000","30","lem")
  add_produk("Odol","1000","30","lem")
  add_produk("Permen","1000","30","lem")
