class Change < ActiveRecord::Migration
  def change
   add_column :users, :date_of_birth, :string
   add_column :users, :age, :integer
   add_column :users, :address, :string
   remove_column:users, :bio_profile, :string
   change_column:countries, :code, :string
   change_column:article, :body, :text
   rename_column:users, :first_name, :user_name
   rename_column:comments, :body, :content
  end
end
